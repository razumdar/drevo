import wtforms
from flask_wtf import FlaskForm
from wtforms.validators import InputRequired, Length, Email, EqualTo


class ContactForm(FlaskForm):
    contacts = wtforms.StringField('Contacts', validators=[InputRequired(), Length(min=1)])
    submit = wtforms.SubmitField('Save')


class WalletForm(FlaskForm):
    wallet = wtforms.StringField('Wallet', validators=[InputRequired()])
    submit = wtforms.SubmitField('Submit')


class EmailVerifyForm(FlaskForm):
    code = wtforms.StringField('Verification code')
    resend = wtforms.SubmitField('Resend')
    submit = wtforms.SubmitField("Submit")


class PaymentForm(FlaskForm):
    transaction = wtforms.StringField('Transaction code')
    submit = wtforms.SubmitField('Submit')
    refresh = wtforms.SubmitField('Reset')


class SendResetForm(FlaskForm):
    email = wtforms.StringField('Email', validators=[Email()])
    submit = wtforms.SubmitField("Send")


class SubmitResetForm(FlaskForm):
    password = wtforms.PasswordField('Пароль:', validators=[
        InputRequired(), EqualTo('confirm', message='Пароли не совпадают'), Length(min=8, max=80)
    ])
    confirm = wtforms.PasswordField('Повторите Пароль:')
    submit = wtforms.SubmitField("Save")
