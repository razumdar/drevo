from flask_wtf import FlaskForm
from werkzeug.security import generate_password_hash
from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import InputRequired, Length, Email, EqualTo, DataRequired, AnyOf

from models.user import User


class LoginForm(FlaskForm):
    username = StringField('Логин', validators=[InputRequired(), Length(min=4, max=15)])
    password = PasswordField('Пароль', validators=[InputRequired(), Length(min=8, max=80)])
    remember = BooleanField('Запомнить меня')


class RegisterForm(FlaskForm):
    email = StringField('Почта:', validators=[InputRequired(), Email(message='Неправильный e-mail'), Length(max=50)])
    username = StringField('Логин:', validators=[InputRequired(), Length(min=4, max=15)])
    password = PasswordField('Пароль:', validators=[InputRequired(), EqualTo('confirm', message='Пароли не совпадают'), Length(min=8, max=80)])
    confirm = PasswordField('Повторите Пароль:')
    parent = StringField('Вас подключает:', validators=[InputRequired(), Length(min=1, max=50)])
    accept_tos = BooleanField('<a href="#" data-toggle="modal" data-target="#agreement">Пользовательское соглашение</a>', validators=[DataRequired(message='Вы не приняли условия пользовательского соглашения')])

    @property
    def hashed_password(self):
        return generate_password_hash(self.password.data, method='sha256')

    def make_user(self) -> User:
        user = User()
        hashed_password = self.hashed_password
        user.username = str(self.username.data).lower()
        user.email = self.email.data
        user.password = hashed_password
        user.parent_id = self.parent.data
        return user
