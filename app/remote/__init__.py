import logging
from typing import Optional

import requests

from models import Transaction


def get_transaction(txhash: str) -> Optional[Transaction]:
    try:
        result = requests.get(
            "https://api.etherscan.io/api?module=proxy&action=eth_getTransactionByHash&"
            "txhash={}&apikey=YourApiKeyToken".format(txhash)
        )
        data: dict = result.json().get('result')
        return Transaction(
            hash=txhash,
            amount=int(data.get('value'), 16),
            sender=data.get('from'),
            receiver=data.get('to')
        )
    except Exception as ex:
        logging.error("Can't parse transaction {}".format(txhash), exc_info=ex)
        return None


if __name__ == "__main__":
    print(get_transaction("0x229913307a23239374d06820beff2cf8b0c87d37af4472e0fc153de2e6239a8a"))
