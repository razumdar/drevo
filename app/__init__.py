from flask import Flask
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from config import config

app = Flask(__name__)
app.config['SECRET_KEY'] = config.secret
app.config['SQLALCHEMY_DATABASE_URI'] = config.database

app.template_folder = config.templates
app.static_folder = config.static

db = SQLAlchemy(app)
migrate = Migrate(app, db)

Bootstrap(app)
from app import views
from models.user import User

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'


@login_manager.user_loader
def load_user(user_id):
    return db.session.query(User).get(int(user_id))
