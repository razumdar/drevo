import datetime
import logging
import math
import smtplib
from email.mime.text import MIMEText
from typing import Optional
from uuid import uuid4

import sqlalchemy as sa
from flask import url_for, render_template, request, session, flash
from flask_login import login_required, logout_user, login_user, current_user as cuser
from werkzeug.security import check_password_hash, generate_password_hash
from werkzeug.utils import redirect

from app import db, app
from app.forms.auth import RegisterForm, LoginForm
from app.forms.profile import WalletForm, EmailVerifyForm, PaymentForm, ContactForm, SendResetForm, SubmitResetForm
from app.remote import get_transaction
from config import config
from models import Transaction
from models.user import User, Payment, ExchangeRate, Content

cuser: User = cuser

logging.getLogger().setLevel(logging.DEBUG)


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = RegisterForm()
    ref = session.get('ref', None)
    parent = None
    any_user = User.query.first()
    if not any_user:
        form.parent.data = '0'
    elif ref:
        parent = User.query.filter(User.ref == ref).first()
    if not parent:
        parent = User.query.order_by(User.created_at).first()
    if parent:
        form.parent.data = str(parent.username)
    if form.validate_on_submit():
        new_user = form.make_user()
        if not any_user:
            new_user.id = 0
            new_user.parent_id = 0
            new_user.keylvl = 14
        elif parent:
            new_user.parent_id = parent.id
        exists = User.query.filter(
            sa.or_(
                User.email == new_user.email,
                User.username == new_user.username
            )
        ).first()
        if exists:
            flash("Такой пользователь уже зарегистрирован.")
            return redirect(url_for('signup'))
        db.session.add(new_user)
        db.session.commit()
        login_user(new_user, remember=True)
        return redirect(url_for('lk'))
    return render_template('signup.jinja2', form=form, parent=parent)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter(
            sa.or_(
                sa.func.lower(User.username) == sa.func.lower(form.username.data),
                sa.func.lower(User.email) == sa.func.lower(form.username.data)
            )
        ).first()
        if user:
            if check_password_hash(user.password, form.password.data):
                login_user(user, remember=form.remember.data)
                return redirect(url_for('lk'))
        flash("Неправильный логин или пароль")
        return redirect(url_for('login'))
    return render_template('login.jinja2', form=form)


@app.route('/contacts', methods=['GET', 'POST'])
@login_required
def contacts():
    form = ContactForm(contacts=cuser.get_contacts_string())
    if form.validate_on_submit():
        cuser.set_contacts_string(form.contacts.data)
        db.session.commit()
        return redirect(url_for('contacts'))
    return render_template('contacts.jinja2', contacts_form=form)


@app.route('/parent')
@login_required
def parent_info():
    parent: User = User.query.get(cuser.parent_id)
    return render_template('parent_info.jinja2', user=cuser, parent=parent)

@app.route('/demoless')
@login_required
def demoless():
    return render_template('demoless.jinja2')


@app.route('/')
def index():
    ref_code = request.args.get('ref', None)
    if ref_code:
        session['ref'] = ref_code
    verification_code = request.args.get('verification', None)
    if verification_code and (not cuser.is_authenticated or not cuser.verified):
        user = User.query.filter(User.verify_code == verification_code).first()
        if user:
            login_user(user, remember=True)
            return redirect(url_for('lk'))
    return render_template('index.jinja2')


@app.route('/dashboard')
@login_required
def dashboard():
    return render_template('dashboard.html', name=cuser.username)


@app.route('/lk2', methods=['GET', 'POST'])
@login_required
def lk():
    wallet_form = WalletForm(wallet=cuser.wallet)
    if wallet_form.validate_on_submit():
        cuser.wallet = wallet_form.wallet.data
        db.session.commit()
    contacts_form = ContactForm(contacts=cuser.get_contacts_string())
    if contacts_form.validate_on_submit():
        cuser.set_contacts_string(contacts_form.contacts.data)
        db.session.commit()
        return redirect(url_for('lk'))
    return render_template('profile.jinja2', current_user=cuser, parent=cuser.get_parent(), wallet_form=wallet_form,
                           contacts_form=contacts_form)


# @app.route('/update_wallet')
# @login_required
# def update_wallet():
#     form = WalletForm()
#     if form.validate_on_submit():
#         cuser.wallet = form.wallet.data
#         db.session.commit()
#     return render_template('profile.jinja2', user=cuser, current_user=cuser, wallet_form=form)


@app.route('/partners')
@login_required
def list_partners():
    wallet_form = WalletForm(**dict(wallet=cuser.wallet))
    partners = User.query.filter(User.parent_id == cuser.id).order_by(User.keylvl)
    return render_template('partners.jinja2', user=cuser, partners=partners, current_user=cuser,
                           parent=cuser.get_parent(), wallet_form=wallet_form)


def find_parent_with_key(user, keylvlbuy=3) -> Optional[User]:
    p = db.session.query(User).filter_by(id=user.parent_id).first()
    if p and p.id != user.id:
        if p.keylvl >= keylvlbuy:
            return p
        else:
            return find_parent_with_key(p, keylvlbuy)
    else:
        return None


@app.route('/buylvl', methods=['POST', 'GET'])
@login_required
def buylvl():
    key2buy = ((cuser.keylvl or 0) + 1)
    level = (cuser.keylvl or 0) + 1
    receiver: User = find_parent_with_key(cuser, level)
    payment: Payment = Payment.query.filter(sa.and_(Payment.payer == cuser.id, Payment.completed == False)).first()
    if not payment:
        payment = prepare_payment(receiver, level)
    form = PaymentForm(transaction=payment.transaction)
    if form.validate_on_submit():
        if form.refresh.data:
            payment.query.delete()
        else:
            payment.transaction = form.transaction.data
        db.session.commit()
        return redirect(url_for('buylvl'))
    return render_template('payment.jinja2', key2buy=key2buy, user=cuser, payment=payment, parent=cuser.get_parent(),
                           receiver=receiver,
                           form=form,
                           wallet_form=WalletForm(**dict(wallet=cuser.wallet)))


##@app.route('/cources')
##@login_required
##def cources():
    ##wallet_form = WalletForm(**dict(wallet=cuser.wallet))
    ##partners = User.query.filter(User.parent_id == cuser.id).order_by(User.created_at)
    ##return render_template('content.jinja2', user=cuser, partners=partners, current_user=cuser,
                          ## parent=cuser.get_parent(), wallet_form=wallet_form)##


@app.route('/marketing')
@login_required
def marketing():
    wallet_form = WalletForm(**dict(wallet=cuser.wallet))
    partners = User.query.filter(User.parent_id == cuser.id).order_by(User.created_at)
    return render_template('marketing.jinja2', user=cuser, partners=partners, current_user=cuser,
                           parent=cuser.get_parent(), wallet_form=wallet_form)


@app.route('/profile')
@login_required
def profile():
    parent = User.query.get(cuser.parent_id)
    wallet_form = WalletForm(**dict(wallet=cuser.wallet))
    partners = User.query.filter(User.parent_id == cuser.id).order_by(User.created_at)
    return render_template('profile.jinja2', user=cuser, partners=partners, current_user=cuser,
                           parent=parent,
                           wallet_form=wallet_form)


@app.cli.command()
def process_payments():
    def process_payment(payment: Payment, rate: ExchangeRate):
        logging.debug('Processing {}'.format(payment))
        if datetime.datetime.now() - payment.created_at > datetime.timedelta(days=1):
            payment.query.delete()
            db.session.commit()
            logging.debug('{} expired.'.format(payment))
        else:
            verify = verify_transaction(payment, rate)
            if verify == 1:
                payment.completed = True
                payment.rejected = False
                user = User.query.get(payment.payer)
                user.keylvl += 1
                db.session.commit()
                send_payment_email(payment)
                logging.debug('{} success.'.format(payment))
            elif verify == 2:
                payment.completed = False
                payment.rejected = True
                db.session.commit()
                logging.debug('{} rejected.'.format(payment))

    payments = Payment.query.filter(sa.and_(Payment.completed == False, Payment.rejected == False)).limit(100).all()
    rate = ExchangeRate.get_actual()
    while len(payments):
        for payment in payments:
            process_payment(payment, rate)
        payments = Payment.query.filter(
            sa.and_(
                Payment.completed == False,
                Payment.rejected == False
            )
        ).limit(100).offset(len(payments)).all()


def rate_getter_coinmarketcap() -> Optional[float]:
    try:
        import requests
        response = requests.get('https://api.coinmarketcap.com/v1/ticker/ethereum/')
        data = response.json()
        return data[0].get('price_usd', None)
    except Exception as ex:
        logging.error("Error getting new rate from coinmarket with {}".format(ex), exc_info=ex)
        return None


@app.cli.command()
def process_exchange_rate(rate_getter=rate_getter_coinmarketcap):
    last_actual = ExchangeRate.get_actual()
    logging.debug("Last actual rate is {}".format(last_actual))
    rate = ExchangeRate()
    rate.usd_per_eth = rate_getter()
    if rate.usd_per_eth is None and last_actual is None:
        logging.debug("Received rate is None. Storing default.")
        rate.usd_per_eth = config.default_rate
    elif rate.usd_per_eth is None and last_actual is not None:
        logging.debug("Received rate is None. Using stored.")
        return
    logging.debug("New rate is {}.".format(rate))
    db.session.add(rate)
    db.session.commit()


def prepare_payment(receiver: User, level: int) -> Payment:
    rate = ExchangeRate.get_actual()
    if rate is None:
        rate = ExchangeRate()
        rate.usd_per_eth = config.default_rate
    payment = Payment()
    payment.payer = cuser.id
    payment.receiver = receiver.id
    payment.receiver_wallet = receiver.wallet
    payment.payer_wallet = cuser.wallet
    payment.created_at = datetime.datetime.now()
    payment.ether = int(math.ceil(rate.to_eth(dollars_per_level(level)) * 10000)) / 10000.0
    payment.dollars = dollars_per_level(level)
    db.session.add(payment)
    db.session.commit()
    return payment


def verify_transaction(payment: Payment, rate: ExchangeRate) -> int:
    """
    :param payment:
    :param rate:
    :return:
    0 - not exists
    1 - success
    2 - exists, but has wrong info
    """
    transaction: Transaction = get_transaction(txhash=payment.transaction)
    if transaction is None:
        return 0
    if transaction.receiver.lower() != payment.receiver_wallet.lower() or transaction.sender.lower() != payment.payer_wallet.lower():
        logging.debug(
            '{} address mismatch. {} != {} or {} != {}'.format(payment, transaction.receiver, payment.receiver_wallet,
                                                               transaction.sender, payment.payer_wallet))
        return 2
    if payment.ether <= transaction.ether() or payment.dollars <= rate.to_usd(transaction.ether()):
        return 1
    logging.debug('{} value mismatch. Expected {} ether (${}), got {} (${})'.format(
        payment, payment.ether, rate.to_usd(payment.ether), transaction.ether(), rate.to_usd(transaction.ether)))
    return 2


DOLLARS_PER_LEVEL = [1] + [20, 40, 60, 100, 160, 260, 420, 680, 1100, 1780, 2880, 4660, 7540, 12200]


def dollars_per_level(level: int) -> float:
    return DOLLARS_PER_LEVEL[level] if level < len(DOLLARS_PER_LEVEL) else DOLLARS_PER_LEVEL[-1]


@app.route('/update_wallet', methods=['POST'])
@login_required
def update_wallet():
    form = WalletForm()
    if form.validate_on_submit():
        cuser.wallet = form.wallet.data
        db.session.commit()
    return redirect(url_for('lk'))


@app.route('/verify_email', methods=['GET', 'POST'])
def verify_email():
    def do_verify():
        cuser.verify_code = None
        cuser.verified = True
        db.session.commit()
        return redirect(url_for('lk'))

    if cuser.verified:  # Пользователя с подтвержденной почтой отправим в кабинет
        return redirect(url_for('lk'))
    if request.args.get('code') is not None:  # Если код есть в ссылке - верифицируем
        user = User.query.filter(User.verify_code == request.args.get('code')).first()
        if user and cuser.is_anonymous:
            login_user(user, True)
        return do_verify()
    if cuser.is_anonymous:  # Если аноним без кода - отправим на авторизацию
        return redirect(url_for('login'))
    form = EmailVerifyForm()
    if form.validate_on_submit():
        if form.resend.data:
            try:
                send_new_verification()
                flash('Письмо отправлено на {}'.format(cuser.email))
            except Exception as ex:
                logging.error("Email error", exc_info=ex)
                flash('Error while sending email')
            return redirect(url_for('verify_email'))
        else:
            if form.code.data == cuser.verify_code:
                return do_verify()
    return render_template('verify_email.jinja2', form=form, user=cuser)


@app.route('/reset_password', methods=['GET', 'POST'])
def reset_password():
    if cuser.is_authenticated:
        return redirect(url_for('lk'))
    user: User = None
    if request.args.get('code'):
        user = User.query.filter(User.verify_code == request.args.get('code')).first()
    send_reset_form = SendResetForm()
    submit_reset_form = SubmitResetForm()
    if user is None:
        if send_reset_form.validate_on_submit():
            email = send_reset_form.email.data
            receiver: User = User.query.filter(sa.and_(
                sa.func.lower(User.email) == email.lower(),
                User.verified == True
            )).first()
            if receiver:
                receiver.verify_code = uuid4().hex
                db.session.commit()
                send_password_reset(receiver)
                flash("Email sent")
            else:
                flash("User not found")
                return redirect(url_for('reset_password'))
            return redirect(url_for('login'))
    else:
        if submit_reset_form.validate_on_submit():
            user.password = generate_password_hash(submit_reset_form.password.data, method='sha256')
            user.verify_code = None
            db.session.commit()
            login_user(user, True)
            return redirect(url_for('lk'))
    return render_template('reset_password.jinja2',
                           user=user,
                           code=request.args.get('code'),
                           send_reset_form=send_reset_form,
                           submit_reset_form=submit_reset_form)


@app.route('/cources', methods=['GET'])
@login_required
def show_content_list():
    return render_template('content.jinja2', content=Content.load_for_level(cuser.keylvl or 0, 0, 100))


@app.route('/content/<int:pk>')
@login_required
def show_content(pk: int):
    content = Content.query.get(int(pk))
    print(content, cuser.keylvl)
    if not content or not content.level <= (cuser.keylvl or 0):
        return redirect(url_for('show_content_list'))
    return render_template('content_item.jinja2', content=content)


def can_resend_verification(user: User):
    return not user.verified and \
           (user.verify_generation_time is None or
            user.verify_generation_time < datetime.datetime.now() + datetime.timedelta(minutes=15))


def send_new_verification():
    if can_resend_verification(cuser):
        cuser.verify_code = uuid4().hex
        cuser.verify_generation_time = datetime.datetime.now()
        db.session.commit()
        send_verification_email(cuser)
    else:
        flash("Вы не можете отправлять письма так часто")


def _send_email(body, subject, recepeints):
    msg = MIMEText(body)
    msg['Subject'] = subject
    msg['From'] = config.mail['address']
    msg['To'] = recepeints
    s = smtplib.SMTP(config.mail['server'], config.mail['port'], timeout=5)
    s.sendmail(msg['From'], recepeints, msg.as_string())


def send_verification_email(user: User):
    _send_email(
        "Подтвердите почту перейдя по ссылке <a href=http://drevoproject.com/{}></a>".format(url_for('verify_email', code=user.verify_code)),
        'Drevo verification',
        user.email
    )


def send_password_reset(user: User):
    _send_email(
        "Перейдите по ссылке для установки нового пароля: <a href=\"http://drevoproject.com{}\"".format(
            url_for('reset_password', code=user.verify_code)
        ),
        'Drevo password reset',
        user.email
    )


def send_payment_email(payment: Payment):
    buyer = User.query.get(payment.payer)
    receiver = User.query.get(payment.receiver)
    _send_email("У вас купили {} уровень за {} эфира".format(buyer.keylvl, payment.ether),
                'Drevo congratulations',
                receiver.email)
