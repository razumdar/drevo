import yaml


class Config(object):
    def __init__(self, path='config.yaml') -> None:
        super().__init__()
        with open(path) as f:
            config = yaml.load(f)
        self.database = config['database']
        self.secret = config['secret']
        self.host = config['host']
        self.port = config['port']
        self.templates = config.get('templates', "../templates")
        self.static = config.get('static', "../static")
        self.default_rate = config.get('default_rate', 300)
        self.mail = config.get('mail', None)


config = Config('config/config.local.yaml')
