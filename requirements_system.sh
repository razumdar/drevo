#!/bin/bash

sudo apt install python3.6
sudo apt install virtualenv
sudo apt install postgresql
virtualenv env /usr/bin/python3.6
source env
pip install -r requirements.txt