$(document).ready(function () {

    // MOBILE BTN


    $(".mobile-btn").on("click", function () {
        $(this).toggleClass("active").animate(500), $(".nav").slideToggle()
    });

    // PAGE SCROLL

    $(".nav li a").on("click", function () {
        var a = $(this).attr("href");
        return 0 != $(a).length && ($(".nav li"),
            $("html, body").animate({scrollTop: $(a).offset().top}, 800)), !1
    });


    // COPY BUTTONS


    $(function () {
        new Clipboard('#copy-button');
    });
    $(function () {
        new Clipboard('.key-btn');
    });


    // SHOW PROFILE CONTACTS FORM

    $('.btn-change').on('click', function () {
        $('.profile-contacts__info').hide();
        $('.profile-contacts form').show('easy');
    });


    $(".btn--mouse").on('click', function () {
        var a = $(this).attr("href");
        return 0 != $(a).length && $("html, body").animate({scrollTop: $(a).offset().top}, 800), !1
    });

    // VIDEO

        $('.video-btn').on('click', function (event) {

            if ($(this).hasClass('play')) {
                $(this).removeClass('play');
                $(this).addClass('pause');

                $('video').get(0).play();

            } else {

                $('.video-btn').removeClass('pause');
                $(this).addClass('play');

                var pl = $('video').get(0);
                pl.pause();

            }
        });


    // SCROLL


    $('.partners-content').slimScroll({
        height: '510px',
        size: '4px',
        railOpacity: 0.5,
        color: '#336e7b'
    });
    $('.contacts-requisites').slimScroll({
        height: '110px',
        size: '2px',
        railOpacity: 0,
        color: '#fff'
    });

    $('.marketing').slimScroll({
        height: '510px',
        size: '4px',
        railOpacity: 0.5,
        color: '#336e7b'
    });


});

