from app import app
from config import config

app.run(host=config.host, port=config.port, debug=True)