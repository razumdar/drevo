class Transaction(object):
    def __init__(self, hash: str, amount: int, sender: str, receiver: str) -> None:
        super().__init__()
        self.hash = hash
        self.amount = amount
        self.sender = sender
        self.receiver = receiver

    def ether(self) -> float:
        return self.amount / 1000000000000000000.0

    def __unicode__(self):
        return "Payment(hash={}, amount={}, sender={}, receiver={})".format(
            self.hash, self.ether(), self.sender, self.receiver
        )

    def __str__(self):
        return self.__unicode__()

    def __repr__(self):
        return self.__unicode__()
