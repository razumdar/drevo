from datetime import datetime
from typing import List
from uuid import uuid4

import sqlalchemy as sa
from flask_login import UserMixin

from app import db


class User(UserMixin, db.Model):
    id = sa.Column(sa.Integer, primary_key=True)
    username = sa.Column(sa.String(15), unique=True)
    email = sa.Column(sa.String(50), unique=True)
    password = sa.Column(sa.String(80))
    parent_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=True)
    keylvl = sa.Column(sa.Integer, primary_key=False, default=0, server_default="0")
    wallet = sa.Column(sa.String(42), unique=False)

    contacts = sa.Column(sa.JSON())

    ref = sa.Column(sa.Text, unique=True, index=True, nullable=False, default=uuid4().hex)

    verified = sa.Column(sa.Boolean, nullable=False, default=False)
    verify_code = sa.Column(sa.Text, nullable=True, default=None)
    verify_generation_time = sa.Column(sa.DateTime(timezone=False))

    created_at = sa.Column(sa.TIMESTAMP(timezone=False), default=datetime.now(), server_default=sa.func.now())

    _internal = {}

    def __init__(self) -> None:
        super().__init__()
        self.created_at = datetime.now()
        self.ref = uuid4().hex
        self.keylvl = 0

    def __repr__(self):
        return "User(id={}, username={}, keylvl={})".format(self.id, self.username, self.keylvl)

    def get_contacts_string(self) -> str:
        return (self.contacts or dict()).get('default', '')

    def set_contacts_string(self, value: str):
        self.contacts = dict(default=value)

    def get_parent(self) -> 'User':
        if 'parent' not in self._internal:
            self._internal['parent'] = User.query.get(self.parent_id)
        return self._internal.get('parent', None)


class Payment(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)

    payer = sa.Column(sa.ForeignKey(User.id))
    receiver = sa.Column(sa.ForeignKey(User.id))

    payer_wallet = sa.Column(sa.Text())
    receiver_wallet = sa.Column(sa.Text())
    ether = sa.Column(sa.Float())
    dollars = sa.Column(sa.Float())

    transaction = sa.Column(sa.Text())
    completed = sa.Column(sa.Boolean(), default=False, server_default='False')
    rejected = sa.Column(sa.Boolean(), default=False, server_default='False')

    created_at = sa.Column(sa.DateTime(timezone=False), server_default=sa.func.now(), default=datetime.now())

    def __unicode__(self):
        return "Payment(id={}, ether={}, payer={}, receiver={})".format(
            self.id, self.ether, self.payer, self.receiver
        )

    def __str__(self):
        return self.__unicode__()

    def __repr__(self):
        return self.__unicode__()


class ExchangeRate(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)

    created_at = sa.Column(sa.DateTime(timezone=False), server_default=sa.func.now(), default=datetime.now())

    usd_per_eth = sa.Column(sa.Float())

    @classmethod
    def get_actual(cls) -> 'ExchangeRate':
        return cls.query.order_by(ExchangeRate.created_at.desc()).first()

    def to_eth(self, usd: float) -> float:
        return usd / self.usd_per_eth

    def to_usd(self, eth: float) -> float:
        return eth * self.usd_per_eth

    def __unicode__(self):
        return "ExchangeRate(id={}, usd_per_eth={})".format(self.id, self.usd_per_eth)

    def __str__(self):
        return self.__unicode__()

    def __repr__(self):
        return self.__unicode__()


class Content(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)
    created_at = sa.Column(sa.DateTime(timezone=False), server_default=sa.func.now(), default=datetime.now())
    level = sa.Column(sa.Integer())

    content = sa.Column(sa.JSON())

    def __unicode__(self):
        return "Content(id={}, level={})".format(self.id, self.level)

    def __str__(self):
        return self.__unicode__()

    def __repr__(self):
        return self.__unicode__()

    @classmethod
    def load_for_level(cls, level: int, offset: int = 0, limit: int = 20) -> 'List[Content]':
        """
        Загружаем контент, доступный определенному уровню пользователя
        :param level: уровень пользователя, для которого мы будем получать контент
        :param offset: количество уже полученных записей
        :param limit: количество записаей "на странице"
        :return:
        """
        return Content.query.filter(Content.level < level).order_by(cls.level, cls.created_at.desc()).limit(
            limit).offset(offset).all()
